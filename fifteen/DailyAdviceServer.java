package fifteen;

import java.net.*;
import java.io.*;

public class DailyAdviceServer{
    String[] adviceList = {"조금식 드세요.", "꼭 맞는 청바지를 입어보세요", "타인을 따돌리는 자, 본인도 겪는 날이 오리라."};
    
    public void go(){
        try{
            ServerSocket serverSock = new ServerSocket(4242);

            while(true){
                Socket sock = serverSock.accept();

                PrintWriter writer = new PrintWriter(sock.getOutputStream());
                String advice = getAdvice();
                writer.println(advice);
                writer.close();
                System.out.println(advice);

            }
        } catch(IOException ex){}
    }

    public String getAdvice(){
        int random = (int) (Math.random() * adviceList.length);
        return this.adviceList[random];
    }

    public static void main(String[] args) {
        DailyAdviceServer server = new DailyAdviceServer();
        server.go();
    }
}