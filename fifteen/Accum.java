package fifteen;

class Accum {

    private static Accum a = new Accum();
    private int counter = 0;

    private Accum(){}

    public static Accum getAccum(){
        if(a == null){
            a = new Accum();
        }
        return a;
    }
        
    public int getCount(){
        return counter;
    }

    public void updateCounter(int add){
        counter += add;
    }

}