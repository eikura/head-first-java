package sixteen;

import java.util.*;

public class TestTree{
    public static void main(String[] args) {
        new TestTree().go();
    }

    public void go(){
        Book b1 = new Book("How Cats Work");
        Book b2 = new Book("Remix your Body");
        Book b3 = new Book("Finding Emo");

        TreeSet<Book> tree = new TreeSet<Book>();
        tree.add(b1);
        tree.add(b2);
        tree.add(b3);
        System.out.println(tree);
    }

    class Book implements Comparable<Book>{
        String title;
        Book(String title){
            this.title = title;
        }

        public int compareTo(Book b){
            return this.title.compareTo(b.getTitle());
        }

        public String getTitle(){
            return this.title;
        }

        public String toString(){
            return this.title;
        }
    }
}