package sixteen;

abstract class Animal{
    protected int size;

    public int getSize(){
        return size;
    }

    public void setSize(int s){
        size = s;
    }

    public void eat(){
        System.out.println("eating");
    }
}