package sixteen;

public class Mountain {
    private String name;
    private int height;

    Mountain(String name, int height){
        this.name = name;
        this.height = height;
    }

    public String getName(){
        return this.name;
    }
    public int getHeight(){
        return this.height;
    }

    public String toString(){
        return this.name + " " + this.height;
    }
}