package six;

import java.util.*;

public class DotCom{
    private String name;
    private ArrayList<String> locationCells;
    
    String checkYourself(String userInput){
        String status = "miss"; // default return value
        int index = locationCells.indexOf(userInput);
        if (index >= 0){
            locationCells.remove(index);
            if(locationCells.isEmpty()){
                status = "kill";
                System.out.println("Ouch! You sunk " + name + "   :  ( ");
            }else{
                status = "hit";
            }
        }
        return status;
    }

    void setLocationCells(ArrayList<String> loc){
        this.locationCells = loc;
    }

    void setName(String name){
        this.name = name;
    }
}