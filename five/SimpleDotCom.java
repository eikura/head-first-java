package five;

class SimpleDotCom{
    private int[] locationCells;
    private int numOfHits;
    
    String checkYourself(String stringGuess){
        String result = "miss"; // default return value

        int guess = Integer.parseInt(stringGuess);
        int index = 0;
        for(int cell: locationCells){
            if(guess == cell){
                result = "hit";
                numOfHits++;
                locationCells[index] = 0;
                break;
            }
            index++;
        } 
        if(numOfHits == locationCells.length){
            result = "kill";
        }
        System.out.println(result);
        return result;
    }

    // return line이 여러 곳에 있어서 혼란스러운 코드
    // return default 값이 무엇인지 한눈에 안들어오는 코드
    // 형변환 가지고 이름이 많이 달라지는거 거슬림
    // String checkYourself(String guess){
    //     int target = Integer.parseInt(guess);
    //     for(int ref : locationCells){
    //         if(ref == target){
    //             numOfHits += 1;
    //             if(numOfHits == locationCells.length){
    //                 return "kill";
    //             }
    //             else{
    //                 return "hit";
    //             }
    //         }
    //     }
    //     return "miss";
    // }

    void setLocationCells(int[] loc){
        this.locationCells = loc;
    }
}