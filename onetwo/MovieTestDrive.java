package onetwo;

class Movie{
    String title;
    String genre;
    int rating;
    
    void playIt(){
        System.out.println("상영합니다.");
    }
}


public class MovieTestDrive{
    public static void main(String[] args){
        
        Movie one = new Movie();

        one.title = "The movie one.";
        one.genre = "genre one";
        one.rating = 4;

        Movie two = new Movie();
        two.playIt();
        two.title = "The movie two.";
        two.genre = "genre two";
        two.rating = 3;
        

    }
}