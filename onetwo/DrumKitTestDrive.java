package onetwo;

class DrumKitTestDrive{
    public static void main(String[] args) {
        
        DrumKit d = new DrumKit();

        d.playSnare();
        d.snare = false;
        d.playTopHat();
        d.topHat = false;

        if(d.snare == true){
            d.playSnare();
        }
        

    }
}