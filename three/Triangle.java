package three;

class Triangle{
    double area;
    int height;
    int length;
    public static void main(String[] args) {
        Triangle[] triangles = new Triangle[4];
        int x = 0;
        while (x < triangles.length){
            triangles[x] = new Triangle();
            triangles[x].height = (x + 1) * 2;
            triangles[x].length = x + 4;
            triangles[x].setArea();
            System.out.print("triangle " + x + ", area");
            System.out.println(" = " + triangles[x].area);
            x = x + 1;
        }
        int y = x;
        Triangle t5 = triangles[2];
        triangles[2].area = 343;
        System.out.print("y = " + y);
        System.out.println(", t5 area = " + t5.area);
    }

    void setArea(){
        this.area = (this.height * this.length) / 2;
    }
}