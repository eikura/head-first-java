package twelve;

import javax.sound.midi.*;
import java.io.*;
import javax.swing.*;
import java.awt.*;

public class MiniMusicPlayer3{
    static JFrame f = new JFrame("My First Music Video");
    static MyInnerDrawPanel ml;

    public static void main(String[] args) {
        MiniMusicPlayer3 mini = new MiniMusicPlayer3();
        mini.go();
    }

    public void setUpGui(){
        ml = new MyInnerDrawPanel();
        f.setContentPane(ml);
        f.setBounds(30, 30, 300, 300);
    }

    public void go(){
        setUpGui();

        try{
            Sequencer sequencer = MidiSystem.getSequencer();
            sequencer.open();

            int[] eventsIWant = {127};
            sequencer.addControllerEventListener(ml, new int[] {127});

            Sequence seq = new Sequence(Sequence.PPQ, 4);
            Track track = seq.createTrack();

            int random = 0;
            for(int i = 0; i < 60 ; i +=4){
                random = (int) ((Math.random() * 50) +1);
                track.add(makeEvent(144,1,random,100,i));

                track.add(makeEvent(176,1,127,0,i));

                track.add(makeEvent(128,1,random,100,i+2));
            }
            sequencer.setSequence(seq);
            sequencer.setTempoInBPM(220);
            sequencer.start();
        } catch(Exception ex){
            ex.printStackTrace();
        }
    }

    public MidiEvent makeEvent(int comd, int chan, int one, int two, int tick){
        MidiEvent event = null;
        try{
            ShortMessage a = new ShortMessage();
            a.setMessage(comd, chan, one, two);
            event = new MidiEvent(a, tick);
        }catch(Exception e){

        }
        return event;
    }

    class MyInnerDrawPanel extends JPanel implements ControllerEventListener {
        boolean msg = false;

        public void controlChange(ShortMessage event){
            msg = true;
            repaint();
        }

        public void paintComponent(Graphics graphics){
            if(msg) {
                Graphics2D g2 = (Graphics2D) graphics;

                int r = (int) (Math.random()*250);
                int g = (int) (Math.random()*250);
                int b = (int) (Math.random()*250);
                
                graphics.setColor(new Color(r,g,b));

                int ht = (int)((Math.random() * 120 + 10));
                int width = (int)((Math.random() * 120 + 10));

                int x = (int)((Math.random() * 40 + 10));
                int y = (int)((Math.random() * 40 + 10));
                
                graphics.fillRect(x,y,ht,width);
                msg = false;
            }
        }
    }
}