package twelve;

import java.awt.*;
import javax.swing.*;

public class MyDrawPanel extends JPanel{
    public void paintComponent(Graphics graphics){

        int r = (int)(Math.random() * 255);
        int g = (int)(Math.random() * 255);
        int b = (int)(Math.random() * 255);
        Color randomColor = new Color(r,g,b);
        graphics.setColor(randomColor);

        graphics.fillOval(70,70,100,100);
    }
}